if [ "FLAVOR" = "mustang" -o "FLAVOR" = "m400" ]; then
	#==========================================
	# create uImage and uInitrd for x-gene
	#------------------------------------------
	/usr/bin/mkimage -A arm -O linux -C none -T kernel -a 0x00080000           \
	                 -e 0x00080000 -n Linux -d /boot/Image /boot/uImage
	/usr/bin/mkimage -A arm -O linux -T ramdisk -C none -a 0 -e 0 -n initramfs \
	                 -d /boot/initrd /boot/uInitrd
fi
